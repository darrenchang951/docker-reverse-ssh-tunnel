#!/bin/sh

eval `ssh-agent -s`;
ssh-add /ssh/id_rsa;
ssh -o "StrictHostKeyChecking no" -AR ${TMP_P}:localhost:${LOCAL_P} ${REMOTE_USR}@${REMOTE_ADDR} "ssh -o 'StrictHostKeyChecking no' -g -N -L ${TUNNEL_P}:localhost:${TMP_P} localhost";

