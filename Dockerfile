FROM alpine

RUN apk add --no-cache openssh
COPY ssh/ ssh/
COPY create_ssh_tunnel.sh create_ssh_tunnel.sh
CMD ./create_ssh_tunnel.sh
