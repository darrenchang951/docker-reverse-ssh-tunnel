# Docker reverse ssh tunnel through VPS
Allow you to expose services on your local device through a VPS.
![ssh-tunnel diagram](doc/diagram.png)

# How to use
- Create `.env` by copying `.env.sample`.
- Modify `.env`
- Place your private key to the remote host in `/ssh/id_rsa`. Make sure it is not password protected.
- Start the container using `docker-compose up --build`.

Great! Now just ssh into your remote host at port `2222` to log into your local host at port `22`. You can do 
`ssh -p 2222 pi@example.com` where `example.com` is your remote host and `2222` is the port used for ssh tunnel.

You can also forward use different ports on your local host, such as port `80/8080` for a web server or port `25565` 
to a bit of fun.    


# Environment variables
- `TMP_P` - temporary port for forwarding back to remote host
- `LOCAL_P` - local SSH server port
- `TUNNEL_P` - the tunnel port on the remote host
- `REMOTE_USR` - the remote host's user name
- `REMOTE_ADDR` - the remote host's address